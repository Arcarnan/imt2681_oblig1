//	Done with help from Jonas S. Solsvik and Halvor Bakken Smedås

// http://localhost:8080/projectinfo/v1/github.com/apache/kafka
//	http://localhost:5000/projectinfo/v1/github.com/apache/kafka
//	https://enigmatic-thicket-15972.herokuapp.com/projectinfo/v1/github.com/apache/kafka
//	https://enigmatic-thicket-15972.herokuapp.com/projectinfo/v1/github.com/JonShard/UnicornsAreReal

package main

/*
import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"io/ioutil"
	"errors"
	"os"
	"reflect"
)

const basePath = "/projectinfo/v1/github.com/"						//  Task given base path
const githubApiCore = "https://api.github.com/repos/"		//  URL for access to github API

type Data struct {				//	where all of the data is stored
	Project   string   			`json: "project"`
	Owner     string   			`json: "owner"`
	TopContributor string   `json: "committer"` //  First one you get from the API will be the one with top contributions
	Contributions   int     `json: "commits"`
	Language  []string 			`json: "languages"`
}

type Projectdata struct{
  Full_Name string			  `json: "full_name"`
  Owner struct {
		Login string 					`json: "login"`
	} 											`json: "owner"`
}

type Contributor struct{
	Login string 					`json: "login"`
	Contributions int		  `json: "contributions"`
}

type Language struct {
	languages map[string]interface{}
}

func GetAPIPath(URL string) (string, error) {
	URLParts:= strings.Split(URL, "/")
	if len(URLParts) !=6 || URLParts[3] != "github.com" {
		return "", errors.New("Parsing error")
	}

	URLPath := githubApiCore + URLParts[4] + "/" + URLParts[5]		//	the path (/projectinfo/v1/[url])
	repoPath := strings.TrimPrefix(URLPath, basePath)						  //	The full API path (https://api.github.com/repos/{owner}/{repo})
	return repoPath, nil
}

func HandlerProjectData(w http.ResponseWriter, r *http.Request) {
	var Responsedata Data
	var projectOwner Data

	var Project Projectdata
	URL:=r.URL.Path
	RepoPath, err := GetAPIPath(URL)
	if err!=nil{
			panic(err.Error)
			status := 400
			http.Error (w, http.StatusText(status), status)
	}
	//JSONBody:= getRequestBody(RepoPath)
	//json.Unmarshal(JSONBody, &Project)
	//Responsedata.Project = "github.com/" + Project.Full_Name
	//Responsedata.Owner = Project.Owner.Login


	var Contributors []Contributor

	/*JSONBody = getRequestBody(RepoPath + "/contributors")
	json.Unmarshal(JSONBody, &Contributors)

	Responsedata.TopContributor = Contributors[0].Login		//  First one you get from the API will be the one with top contributions
	Responsedata.Contributions = Contributors[0].Contributions
*/

//JSONBody := getRequestBody(RepoPath + "/languages")
//var Languages Language
//Languages:=Language{}
//json.Unmarshal([]byte(JSONBody), &Languages.languages);
/*
	LanguageMap := make(map [string] int)
	json.Unmarshal(JSONBody, &LanguageMap);
	for key:=range LanguageMap{
		Languages = append(Languages, "'" + key + "'")
	}*/

//Responsedata.Language = Languages

/*

	projectOwner, err = getProjectData(Project, Contributors, Languages, RepoPath)
	fmt.Fprintln(w, "-------------\tProject info\t-------------\n")
	fmt.Fprintln(w, "Project: " + projectOwner.Project)
	fmt.Fprintln(w, "Owner: " + projectOwner.Owner)
	fmt.Fprintln(w, "Top contributor: "+projectOwner.TopContributor)
	fmt.Fprint(w, "Contributions: ")
	fmt.Fprint(w, projectOwner.Contributions)


	fmt.Fprint(w, "\nLanguage(s): ")
	fmt.Fprintln(w, Responsedata.Language)

	fmt.Fprint(w, "\n-------------\tWritten as JSON\t-------------\n\n")
	resultat, err:=json.Marshal(Responsedata)
	if err !=nil {
		panic(err.Error)
	}
	fmt.Fprint(w, string(resultat))
}

func getProjectData (P Projectdata, C []Contributor, L Language, RepoPath string) (Data, error){
	var responseData2 Data
	//var Languages []Language

	JSONBody:= getRequestBody(RepoPath)
	json.Unmarshal(JSONBody, &P)

	responseData2.Project = "github.com/" + P.Full_Name
	responseData2.Owner = P.Owner.Login
	fmt.Println(responseData2.Owner)

	JSONBody = getRequestBody(RepoPath + "/contributors")
	json.Unmarshal(JSONBody, &C)

	responseData2.TopContributor = C[0].Login		//  First one you get from the API will be the one with top contributions
	responseData2.Contributions = C[0].Contributions


	//LanguageMap := make(map [string] int)
	L=Language{}
	JSONBody = getRequestBody(RepoPath + "/languages")
	json.Unmarshal([]byte(JSONBody), &L.languages);


	keys := reflect.ValueOf(L.languages).MapKeys()
	for _, key:=range keys{
		responseData2.Language = append(responseData2.Language, key.Interface().(string))
	}

	//responseData2.Language = Languages

	return responseData2, nil
}

func getRequestBody (repoPath string)[]byte{
  response, err:= http.Get(repoPath)
	if err !=nil{
		panic(err.Error)
	}
  body, err:= ioutil.ReadAll(response.Body)
	if err !=nil{
		panic(err.Error)
	}
  response.Body.Close()

  return body
}

func main() {
	http.HandleFunc(basePath, HandlerProjectData)
	log.Println("http.ListenAndServe", http.ListenAndServe(":" +os.Getenv("PORT"), nil))	//	this is used to run online (heroku)
}
*/
