package main

//	For the first test, i got help from someone in class. I do unfortunately not know his name
import (
	"testing"
)

func TestGetAPIPath(t *testing.T) { //  test to check if the correct API path is provided
	var input = []string{"/projectinfo/v1/github.com/JonShard/UnicornsAreReal", "/projectinfo/v1/github.com/apache/kafka"}
	var output = []string{"https://api.github.com/repos/JonShard/UnicornsAreReal", "https://api.github.com/repos/apache/kafka"}

	for i := range input {
		fromInput, err := getAPIPath(input[i])
		if fromInput != output[i] {
			t.Fatalf("ERROR - Expected: %s - Got: %s", output[i], fromInput)
		}
		if err != nil {
			panic(err.Error)
		}
	}
}

//	I made this test with help from Sindre Blomberg Garvik
func TestGetProjectData(t *testing.T) {
	var realData Data
	var testProjectdata Projectdata
	var testCondtibutors []Contributor
	var testCondtibutorsSingle Contributor
	var repoPath = "https://enigmatic-thicket-15972.herokuapp.com/projectinfo/v1/github.com/JonShard/UnicornsAreReal"

	testProjectdata.FullName = "JonShard/UnicornsAreReal"
	testProjectdata.Owner.Login = "JonShard"

	testCondtibutorsSingle.Login = "JonShard"
	testCondtibutorsSingle.Contributions = 57
	testCondtibutors = append(testCondtibutors, testCondtibutorsSingle)

	realData.Project = "JonShard/UnicornsAreReal"
	realData.Owner = "JonShard"
	realData.TopContributor = "JonShard"
	realData.Contributions = 57

	projectData, err := getProjectData(testProjectdata, testCondtibutors, repoPath)
	if err != nil {
		panic(err.Error)
	}

	if projectData.Project != realData.Project {
		t.Error("projectData Project not made properly. " + projectData.Project + " - " + realData.Project)
	}

	if projectData.Owner != realData.Owner {
		t.Error("projectData Owner not made properly. " + projectData.Owner + " - " + realData.Owner)
	}

	if projectData.TopContributor != realData.TopContributor {
		t.Error("projectData TopContributor not made properly. " + projectData.TopContributor + " - " + realData.TopContributor)
	}

	if projectData.Contributions != realData.Contributions {
		t.Error("projectData Contributions not made properly. ")
	}
}
