//	Done with help from Jonas S. Solsvik, Sindre Blomberg Garvik and Halvor Bakken Smedås

//  http://localhost:5000/projectinfo/v1/github.com/apache/kafka
//	http://localhost:5000/projectinfo/v1/github.com/apache/JonShard/UnicornsAreReal
//	https://enigmatic-thicket-15972.herokuapp.com/projectinfo/v1/github.com/apache/kafka
//	https://enigmatic-thicket-15972.herokuapp.com/projectinfo/v1/github.com/JonShard/UnicornsAreReal

package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

const basePath = "/projectinfo/v1/github.com/"        //  Task given base path
const githubAPICore = "https://api.github.com/repos/" //  URL for access to github API

//Data - where all of the data is stored
type Data struct {
	Project        string   `json:"project"`
	Owner          string   `json:"owner"`
	TopContributor string   `json:"committer"` //  First one you get from the API will be the one with top contributions
	Contributions  int      `json:"commits"`
	Language       []string `json:"language"`
}

//Projectdata - fetching name for repo and owner
type Projectdata struct {
	FullName string `json:"name"` //	for some reason, i have to call it "Full_Name" in order to get the correct data
	Owner    struct {
		Login string `json:"login"`
	} `json:"owner"`
}

//Contributor - fetching name and number for top contributor
type Contributor struct {
	Login         string `json:"login"`
	Contributions int    `json:"contributions"`
}

func getAPIPath(URL string) (string, error) {
	URLParts := strings.Split(URL, "/")
	if len(URLParts) != 6 || URLParts[3] != "github.com" { //	removing len gives: "desc":"TestAPI/owner/repo TBfree","ok":false,"details":"Incorrect field value: owner.
		log.Println("getAPIPath parsing error. parsing failed.  ")
		return "", errors.New("Parsing error")
	}

	URLPath := githubAPICore + URLParts[4] + "/" + URLParts[5] //	the path (/projectinfo/v1/[url])
	repoPath := strings.TrimPrefix(URLPath, basePath)          //	The full API path (https://api.github.com/repos/{owner}/{repo})
	return repoPath, nil
}

func handlerProjectData(w http.ResponseWriter, r *http.Request) {
	var Responsedata Data
	var partialResponseData Data
	var Project Projectdata
	var Contributors []Contributor
	var Languages []string

	URL := r.URL.Path
	RepoPath, err := getAPIPath(URL)
	if err != nil { //	TODO is this the function that is not working?
		status := http.StatusBadRequest //	changing this gives same error, but with a different character
		http.Error(w, http.StatusText(status), status)
		return
	}

	partialResponseData, err = getProjectData(Project, Contributors, RepoPath) //	made a separate function in order to get enough test coverage
	if err != nil {
		status := http.StatusBadRequest
		http.Error(w, http.StatusText(status), status)
		return
	}
	Responsedata = partialResponseData

	JSONBody := getRequestBody(RepoPath + "/languages") //	I gave up on getting the language map over in the getProjectData function
	LanguageMap := make(map[string]int)
	json.Unmarshal(JSONBody, &LanguageMap)
	for key := range LanguageMap {
		Languages = append(Languages, key)
	}
	Responsedata.Language = Languages

	/*fmt.Fprintln(w, "-------------\tProject info\t-------------")
	fmt.Fprintln(w, "\nProject: "+Responsedata.Project)
	fmt.Fprintln(w, "Owner: "+Responsedata.Owner)
	fmt.Fprintln(w, "Top contributor: "+Responsedata.TopContributor)
	fmt.Fprint(w, "Contributions: ")
	fmt.Fprint(w, Responsedata.Contributions)
	fmt.Fprint(w, "\nLanguage(s): ")
	fmt.Fprintln(w, Responsedata.Language)
	fmt.Fprint(w, "\n-------------\tWritten as JSON\t-------------\n\n")*/
	//resultat, err := json.Marshal(Responsedata)
	w.Header().Set("Content-Type", "application/JSON")
	err = json.NewEncoder(w).Encode(Responsedata)
	if err != nil {
		status := http.StatusBadRequest
		http.Error(w, http.StatusText(status), status)
		return
	}
	//fmt.Fprint(w, string(resultat))
}

func getProjectData(P Projectdata, C []Contributor, RepoPath string) (Data, error) {
	var responseData2 Data

	JSONBody := getRequestBody(RepoPath)
	json.Unmarshal(JSONBody, &P)
	responseData2.Project = P.FullName
	responseData2.Owner = P.Owner.Login

	JSONBody = getRequestBody(RepoPath + "/contributors")
	json.Unmarshal(JSONBody, &C)
	responseData2.TopContributor = C[0].Login //  First one you get from the API will be the one with top contributions
	responseData2.Contributions = C[0].Contributions

	return responseData2, nil
}

func getRequestBody(repoPath string) []byte {
	response, err := http.Get(repoPath)
	if err != nil {
		log.Println("getRequestBody, response, error. ")
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("getRequestBody, response, error. ")
	}
	//response.Body.Close()
	return body
}

func main() {
	http.HandleFunc(basePath, handlerProjectData)
	log.Println("http.ListenAndServe", http.ListenAndServe(":"+os.Getenv("PORT"), nil)) //	this is used to run online (heroku)
}
